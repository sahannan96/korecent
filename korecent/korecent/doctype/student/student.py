# Copyright (c) 2024, Korecent and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class Student(Document):
    def before_save(self):
        for student in self.input_table:
            if student.is_new():
                if frappe.form_dict.get("web_form"):
                    self.append("output_table", {"web": student.student_name})
                else:
                    self.append("output_table", {"desk": student.student_name})
